Crafty.init(600, 400, document.getElementById("game"));
function getCircleCoordinates(radius){
	var coordinates = [];
	var increment = 0.5; //the smaller the increment the higher the accuracy
	if(radius <= 0){
		console.log("Error radius must be greater than 0");
		return;
	}
	for(var x = -1 * radius; x < radius; x += increment){
		y = Math.sqrt(radius * radius - x * x);
		coordinates.push(radius + x); //add the radius to shift the center
		coordinates.push(radius + y);
	}
	for(var x = radius; x > -1 * radius; x-= increment){
		y = Math.sqrt(radius * radius - x * x);
		coordinates.push(radius + x);
		coordinates.push(radius + -1 * y);
	}
	return coordinates;
}

Crafty.background('#FFFF00')
var top = Crafty.e('2D, Canvas, Color, Wall')
    .attr({x:0, y:0, h:10, w:600})
    .color('blue');
    
var floor = Crafty.e('2D, Canvas, Color, Wall')
    .attr({x:0, y:390, h:10, w:600})
    .color('blue');
    
var right = Crafty.e('2D, Canvas, Color, Wall')
    .attr({x:590, y:0, h:400, w:10})
    .color('blue');
    
var left = Crafty.e('2D, Canvas, Color, Wall')
    .attr({x:0, y:0, h:400, w:10})
    .color('blue');

var poly = Crafty.e('2D, Canvas, Collision, WiredHitBox, Wall')
    .attr({x:10, y:10, w:500, h:500})
    .debugStroke("red")
    .collision([100, 100, 100, 300,  500, 250, 500, 50]);
Crafty.sprite("images/ball.png", {Ball: [0,0,50,50]});
var norm = Crafty.math.Vector2D();
var ball = Crafty.e('2D, Canvas, Collision, Motion, WiredHitBox, Ball')
    .attr({x:510, y:310, w:50, h:50})
    .debugStroke('red')
    .checkHits('Wall')
    .bind('HitOn', function() {
      vel.x = 0;
      vel.y = 0;
      
    })
    .bind('EnterFrame', function(){
      Crafty.e('2D, Canvas, Color')
        .attr({x:ball.x + 25, y:ball.y + 25 , w:1, h:1})
        .color('black');
    })
    var circleCol = getCircleCoordinates(ball.attr("w") / 2);
		ball.collision(circleCol);
    var vel = ball.velocity();
    vel.x = -10;
    vel.y = -10;